#+TITLE: DFS demo artefact(HTML)
#+AUTHOR: VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document builds the =DFS demo= interactive
  artefact(HTML)

* Features of the artefact
+ Artefact provides a demo module for DFS Algorithm
+ User can see a randomly generate graph or work on same
  graph as many times as he need.
+ User can click the =New Graph= button to work on a new
  graph at any time.
+ User can click the =Reset= button to work on same graph at
  any time.

* HTML Framework of DFS
** Head Section Elements
Title, meta tags and all the links related to Js, CSS of
this artefact comes under this section.
#+NAME: head-section-elems
#+BEGIN_SRC html
<title>DFS Demo</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="./../../static/libs/vis.min.css" rel="stylesheet" type="text/css" />
<link href="./../css/dfs.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://ds1-iiith.vlabs.ac.in/exp-common-css/common-styles.css">
#+END_SRC

** Body Section Elements
*** Instruction box
This is an instruction box, in which the instructions needed
for the user to perform the experiments are displayed in a
form of list.
#+NAME: instruction-box
#+BEGIN_SRC html
<div class="instruction-box">
  <button class="collapsible">Instructions</button>
  <div class="content">
    <ul>
      <li>Click on the graph to enable the border.</li>
      <li>Click on the node which you want to select as source node of the DFS demo.</li>
      <li>You can click on <b>Pause</b> or <b>Play</b> to do so respectively and adjust speed using the slider.</li>
      <li>Click on <b>Next</b> to move a step forward.</li>
      <li>Click on <b>New Graph</b> to start over with a new graph.</li>
      <li>Click on <b>Reset</b> to start over with the same graph.</li>
      <li>Zoom or Adjust graph by dragging nodes according to your convenience.</li>  
    </ul>
  </div>
</div>

#+END_SRC

*** Legend
This is the div used to represent the legend of the nodes
and edges used
#+NAME: legend
#+BEGIN_SRC html
<div id="legend" >
  <ul class="legend">
    <li><span class="blue"></span> : Source Node</li>
    <li><span class="white"></span> : Unvisited Node</li>
    <li><span class="grey"></span> : Current Node</li>
    <li><span class="green"></span> : Traversed Node</li>
    <li><span class="black"></span> : Explored Node</li>
  </ul>
</div>
<div id="legend_edge" >
  <ul class="legend">
    <li><span class="grey_edge"></span> : Edge not added to DFS path </li>
    <li><span class="black_edge"></span> : Edge added to DFS path </li>
  </ul>
</div>

#+END_SRC

*** Graph Holder
This is a graph holder, which contains graph in canvas where
the graphs are randomly created
#+NAME: graph-holder
#+BEGIN_SRC html
<center>   
  <div id="graph"></div> 
</center>

#+END_SRC

*** Slider
This is a slider to which =change_interval= method is
appended to control the demonstration speed of an artefact.
#+NAME: slider
#+BEGIN_SRC html
<center>
  <div>
    <p id="ranger">
      Min.Speed <input type="range" min="100" max="2500" id="interval" class="slider_input" value="1500" onclick="change_interval();" onchange="change_interval();"> Max.Speed
    </p>
  </div>
</center>

#+END_SRC

*** Comments Box
This is a box in which the comments gets displayed based on
what is happening currently in the artefact.
#+NAME: comments-box
#+BEGIN_SRC html
<div id="comments-demo" class="comment-box">
  <b>Observations:</b><br>
  <p id="ins"> </p>
</div>

#+END_SRC

*** Controller Elements
This are the controller buttons(Start, Reset, Pause) of an
artefacts used for performing/demonstrating the artefact.
#+NAME: controller-elems
#+BEGIN_SRC html
<div class="buttons">
  <input type="button" value="Reset" class="button-input" id="reset">
  <input type="button" value="New graph" class="button-input" id="New_graph">
  <input type="button" value="Pause" class="button-input" id="pause">
  <input type="button" value="Start" class="button-input" id="start">
</div>

#+END_SRC

*** JS Elements
Contains the JS scripts used for the experiment
#+NAME: js-elems
#+BEGIN_SRC js
<script src="./../../static/libs/vis.min.js"></script>
<script src="./../js/Algo.js"></script> 
<script src="./../js/data.js"></script> 
<script src="./../js/dfs-demo.js"></script> 
<script src="https://ds1-iiith.vlabs.ac.in/exp-common-js/instruction-box.js"></script>

#+END_SRC

* Tangle
#+BEGIN_SRC html :tangle dfs-demo.html :eval no :noweb yes
<!DOCTYPE HTML>
<html lang="en">
  <head>
    <<head-section-elems>>
  </head>
  <body>
    <<instruction-box>> 
    <<graph-holder>>
    <<legend>>
    <<slider>>
    <<comments-box>>
    <<controller-elems>>
    <<js-elems>>
  </body>
</html>
#+END_SRC



