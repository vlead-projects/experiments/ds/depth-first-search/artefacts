#+TITLE: 
#+AUTHOR:VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
This has common styles for different types of buttons used across the experiments.
* Button
Graph Generate Button CSS
Green Colored buttons with curvy corners
#+NAME: button
#+BEGIN_SRC css
button {
	background-color: #4CAF50;
	border: none;
	color: white;
	/* padding: 1% 2%; */
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 10px;
	/* margin: 4px 2px; */
	cursor: pointer;
	background-color: white;
	border-radius: 2vw;
	border-style: solid;
	/* border-width: 1px; */
	border-color: rgb(158, 208, 62);
	background-color: rgb(158, 208, 62);
	position: absolute;
	left: 45%;
	top: 70%;
	height: 9vh;
	width: 11vw;
}
#+END_SRC

* Green Button
Class of buttons with green color
#+NAME: green
#+BEGIN_SRC css
.green {
	border-color: rgb(158, 208, 62);
	background-color: rgb(158, 208, 62);
}
#+END_SRC

* Blue Button
Class of buttons with blue color
#+NAME: blue
#+BEGIN_SRC css
.blue {
	border-color: rgba(105, 179, 215, 1.0);
	background-color: rgba(105, 179, 215, 1.0);
	top: 85%;
}
#+END_SRC

* Tangle                                      
#+BEGIN_SRC css :tangle buttons.css :eval no :noweb yes
<<button>>
<<green>>
<<blue>>
#+END_SRC
